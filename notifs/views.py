import json

import requests
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import send_mail
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views import View
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from notifs import app_settings
from notifs.app_settings import get_admin_email, get_kn_template_name
from notifs.forms import AdminSendNotifForm
from notifs.models import NotifConfig
from .helpers import CustomViewContextMixin, merge_two_dicts


def send_notif(key, recipient_type, recipient, body_values, extra_data, **kwargs):
    """
    usage: for notif after certain actions call this function
    send_notif(key='some_key', recipient_type='single', recipient=user,
                          extra_data={"click_action": "splash_activity"},
                          body_values=[user.fullname])
                          
    if recipient_type == 'single' --> recipient is a user object
    if recipient_type == 'multiple' --> recipient is a user queryset
    """

    if 'notif_config' in kwargs:
        notif_config = kwargs['notif_config']

    else:
        try:
            notif_config = NotifConfig.objects.get(key=key)
        except NotifConfig.DoesNotExist:
            return False, _('Notif config not found')

    if notif_config.is_valid:

        notif_title = notif_config.active_title
        notif_body = notif_config.body(body_values)

        if notif_config.sms:
            SmsNotif(recipient, notif_body)

        if notif_config.push:
            FirebaseNotif(recipient_type, recipient, notif_title, notif_body, extra_data)

        if notif_config.email:
            EmailNotif(recipient, notif_title, notif_body)

        if notif_config.pushe:
            PusheNotif(recipient_type, recipient, notif_title, notif_body)

        return True, _('Notif sent')

    return False, _('Invalid key')


class EmailNotif(object):
    def __init__(self, recipient, notif_title, notif_body):
        self.user = recipient

        self.email_subject = notif_title
        self.email_content = notif_body

        self.send_email()

    @property
    def user_email(self):
        return self.user.email if self.user.email else None

    def send_email(self):
        if self.user_email:
            self.send_request()

    def send_request(self):
        admin_email = get_admin_email()

        send_mail(self.email_subject, self.email_content, admin_email, [self.user_email, ])


class SmsNotif(object):
    # todo support multiple
    def __init__(self, recipient, notif_body):
        self.user = recipient
        self.sms_content = notif_body

        self.send_sms()

    @property
    def user_phone(self):
        return self.user.phone if self.user.phone else None

    def send_sms(self):
        if not self.user_phone:
            return False

        self.send_request()

    def send_request(self):
        sms = requests.post('https://api.kavenegar.com/v1/' + app_settings.KAVENEGAR_API + '/sms/send.json',
                            data={'message': self.sms_content, 'receptor': self.user_phone})
        sms_result = sms.json()
        sms_return = sms_result.get('return')

        self.sms_status_code = sms_return['status']
        self.sms_success = True if self.sms_status_code == 200 else False

    @classmethod
    def send_template_sms(cls, token, receptor, key):
        template = get_kn_template_name(key)

        # template options: verify --> the name must be set in kavenegar site
        sms = requests.post('https://api.kavenegar.com/v1/' + app_settings.KAVENEGAR_API + '/verify/lookup.json',
                            data={'token': token, 'template': template, 'receptor': receptor})
        sms_result = sms.json()
        sms_return = sms_result.get('return')

        return sms_return


class FirebaseNotif(object):
    def __init__(self, recipient_type, recipient, notif_title, notif_body, extra_data):
        self.user = recipient
        self.recipient_type = recipient_type
        self.extra_data = extra_data
        self.push_title = notif_title
        self.push_body = notif_body

        self.send_fcm()

    def send_fcm(self):
        if self.user.firebase_token:
            recipient_data = self.create_recipient_data()
            payload = self.create_payload(recipient_data)
            self.send_request(payload)

    def send_request(self, payload):
        headers = {
            "Content-Type": "application/json",
            "Authorization": "key=" + app_settings.FIREBASE_SERVER_KEY,
        }

        r = requests.post('https://fcm.googleapis.com/fcm/send', headers=headers,
                          data=json.dumps(payload))

        self.status_code = r.status_code

    def create_payload(self, recipient_data):
        payload = {
            "data": {
                "title": self.push_title,
                "body": self.push_body
            }
        }

        if self.extra_data:
            payload['data'].update(self.extra_data)

        return merge_two_dicts(payload, recipient_data)

    def create_recipient_data(self):
        if self.recipient_type == 'single':
            data = {
                "to": self.user.firebase_token
            }

        elif self.recipient_type == 'multiple':
            firebase_tokens_li = list(self.user.values_list('firebase_token', flat=True))

            data = {
                "registration_ids": firebase_tokens_li
            }

        elif self.recipient_type == 'all':
            pass

        else:
            return 'Fail'

        return data


class PusheNotif(object):
    def __init__(self, recipient_type, recipient, notif_title, notif_body):
        self.user = recipient
        self.recipient_type = recipient_type
        self.push_title = notif_title
        self.push_body = notif_body
        self.send()

    def send(self):
        headers = self.create_headers()
        recipient_data = self.create_recipient_data()
        payload = self.create_payload(recipient_data)
        self.send_request(headers, payload)

    def create_headers(self):
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Token " + app_settings.PUSHE_TOKEN,
        }

        return headers

    def create_recipient_data(self):
        if self.recipient_type == 'single':
            recipient_data = {
                "app_ids": [app_settings.ANDROID_PACKAGE_NAME],
                "filters": {
                    "pushe_id": [self.user.pushe_id]
                },
            }

        elif self.recipient_type == 'multiple':
            pushe_ids_li = list(self.user.values_list('pushe_id', flat=True))

            recipient_data = {
                "app_ids": [app_settings.ANDROID_PACKAGE_NAME],
                "filters": {
                    "pushe_id": pushe_ids_li
                }
            }

        elif self.recipient_type == 'all':
            recipient_data = {
                "app_ids": [app_settings.ANDROID_PACKAGE_NAME],
            }

        else:
            return 'Fail'

        return recipient_data

    def create_payload(self, recipient_data):
        payload = {
            "data": {
                "title": self.push_title,
                "content": self.push_body
            }
        }

        return merge_two_dicts(payload, recipient_data)

    def send_request(self, headers, payload):
        r = requests.post('https://api.pushe.co/v2/messaging/notifications/', headers=headers,
                          data=json.dumps(payload))

        self.status_code = r.status_code


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def save_firebase_token_view(request):
    """
    requirement: user must have a firebase_token field
    """
    user = request.user

    firebase_token = request.POST.get('firebase_token')
    user.refresh_firebase_token(firebase_token)

    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def save_pushe_id_view(request):
    """
    requirement: user must have a firebase_token field
    """
    user = request.user

    pushe_id = request.POST.get('pushe_id')
    user.refresh_pushe_id(pushe_id)

    return Response(status=status.HTTP_200_OK)


@method_decorator(staff_member_required, name='dispatch')
class AdminSendNotifView(CustomViewContextMixin, View):
    """
    Base setting class for list of editable configs 
    just pushe is supported for now
    """

    template = 'send_notif.html'
    form_class = AdminSendNotifForm
    redirect_url_name = 'admin_send_notif'
    successful_message = _('Sent')

    def get(self, request):
        self.set_context(request)

        form = self.form_class()
        self.update_context({
            'form': form,
        })

        return render(request, self.template, self.context)

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            obj = NotifConfig(key='admin_custom',
                              custom_title=form.cleaned_data['custom_title'],
                              custom_body=form.cleaned_data['custom_body'],
                              pushe=True,
                              is_enable=True)

            sent, message = send_notif(key='admin_custom', recipient_type='multiple', recipient=form.cleaned_data['user'],
                                       extra_data={}, body_values=[], notif_config=obj)
            if sent:
                messages.add_message(request, messages.SUCCESS, message)

            else:
                messages.add_message(request, messages.ERROR, message)

        return redirect(self.redirect_url_name)