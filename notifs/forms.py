# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib.auth import get_user_model

from notifs.models import NotifConfig

User = get_user_model()


class AdminSendNotifForm(forms.ModelForm):
    user = forms.ModelMultipleChoiceField(queryset=User.objects.all())

    class Meta:
        model = NotifConfig
        fields = ['custom_title', 'custom_body', 'user']