# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin


def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z


class CustomViewContextMixin:
    """
    context handler, in creating new admin custom view inherit this mixin
    """

    def set_context(self, request):
        self.context = admin.site.each_context(request)
        self.extra_context()

    def extra_context(self):
        return self.context.update({})

    def update_context(self, dict):
        self.context.update(dict)