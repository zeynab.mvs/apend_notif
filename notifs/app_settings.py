from django.conf import settings

FIREBASE_SERVER_KEY = getattr(settings, 'FIREBASE_SERVER_KEY')
KAVENEGAR_API = getattr(settings, 'KAVENEGAR_API')
ADMIN_EMAIL = getattr(settings, 'ADMIN_EMAIL')
DEFAULT_KAVENEGAR_TEMPLATE = getattr(settings, 'DEFAULT_KAVENEGAR_TEMPLATE')
PUSHE_TOKEN = getattr(settings, 'PUSHE_TOKEN')
ANDROID_PACKAGE_NAME = getattr(settings, 'ANDROID_PACKAGE_NAME')


def get_kn_template_name(key):
    if 'configs' in settings.INSTALLED_APPS:
        from configs.models import Config

        try:
            obj = Config.objects.get(key=key)
            return obj.value
        except Config.DoesNotExist:
            pass

    return DEFAULT_KAVENEGAR_TEMPLATE


def get_admin_email():
    if 'configs' in settings.INSTALLED_APPS:
        from configs.models import Config

        config = Config.objects.safe_get('admin_email')
        config_value = config.value

        if config_value:
            return config_value
        else:
            return ADMIN_EMAIL