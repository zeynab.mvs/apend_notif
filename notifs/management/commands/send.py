from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from notifs.models import NotifConfig
from notifs.views import send_notif

User = get_user_model()


class Command(BaseCommand):
    help = 'Test command'

    def add_arguments(self, parser):
        parser.add_argument('username', help='Username')

    def handle(self, *args, **kwargs):
        username = kwargs['username']

        user = User.objects.get(username=username)

        # y, message = send_notif(key='notif_ad_declined', recipient_type='single', recipient=user,
        #                extra_data={}, body_values=['title of ad'])

        obj = NotifConfig(key='admin_custom', custom_title='hi1133', custom_body='hello1133', pushe=True, is_enable=True, )
        y, message = send_notif(key='admin_custom', recipient_type='single', recipient=user,
                                extra_data={}, body_values=[], notif_config=obj)

        print(y, message)