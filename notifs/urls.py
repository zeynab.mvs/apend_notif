# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import path

from . import views

urlpatterns = [

    # api urls
    path('api/v1/firebase/save/', views.save_firebase_token_view),
    path('api/v1/pushe/save/', views.save_pushe_id_view),

    path('admin/notif/send/', views.AdminSendNotifView.as_view(), name='admin_send_notif'),

]