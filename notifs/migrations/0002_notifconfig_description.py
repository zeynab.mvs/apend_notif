# Generated by Django 2.2.4 on 2019-09-08 13:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='notifconfig',
            name='description',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Description'),
        ),
    ]
