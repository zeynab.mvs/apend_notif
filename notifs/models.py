# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext as _


class NotifConfig(models.Model):
    key = models.CharField(_('Key'), max_length=255, unique=True, default='', blank=False,
                           help_text=_('Enter in english, must be unique'))
    def_title = models.CharField(_('Default title'), max_length=255)
    def_body = models.TextField(_('Default body'), max_length=255)
    custom_title = models.CharField(_('Custom title'), max_length=255, default='', blank=True)
    custom_body = models.TextField(_('Custom body'), max_length=255, default='', blank=True)
    description = models.CharField(_('Description'), max_length=255, default='', blank=True)
    is_enable = models.BooleanField(_('Is enable?'), default=True)
    sms = models.BooleanField(_('Sms'), default=False)
    push = models.BooleanField(_('Push'), default=False)
    pushe = models.BooleanField(_('Pushe'), default=False)
    email = models.BooleanField(_('Email'), default=False)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = _('Notif Config')
        verbose_name_plural = _('Notif Configs')

    @property
    def is_valid(self):
        if not self.is_enable:
            return False

        if not self.sms and not self.push and not self.email and not self.pushe:
            return False

        return True

    @property
    def active_title(self):
        return self.custom_title if self.custom_title else self.def_title

    @property
    def active_body(self):
        return self.custom_body if self.custom_body else self.def_body

    def body(self, body_values):
        text = self.active_body

        # maximum valid tokens is two for now
        if len(body_values) == 0:
            w = text.format()
        elif len(body_values) == 1:
            w = text.format(body_values[0])
        elif len(body_values) == 2:
            w = text.format(body_values[0], body_values[1])
        else:
            return False

        return ''.join((u"\u200F", w))

    def clean(self):
        if not self.pushe and not self.push and not self.sms and not self.email:
            raise ValidationError(_('Choose one of actions: sms / push / pushe / email'))