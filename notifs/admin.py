# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from notifs.models import NotifConfig


class NotifConfigAdmin(admin.ModelAdmin):
    list_display = ['id', 'is_enable', 'key', 'description', 'active_title', 'active_body']
    list_filter = ['is_enable', 'push', 'pushe', 'sms', 'email']
    list_display_links = ['id', 'key']
    search_fields = ['key']
    readonly_fields = ['def_title', 'def_body']


admin.site.register(NotifConfig, NotifConfigAdmin)