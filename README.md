Notifs
======

Quick start
-----------

1. Add "notifs" to your INSTALLED_APPS setting like this::

```bash
    INSTALLED_APPS = [
        ...
        'notifs',
        ...
    ]

```

2. Include the polls URLconf in your project urls.py like this::

```python
from django.urls import include, path

path('', include('notifs.urls')),
```



3. Run migration
```bash
python manage.py migrate

```

Requirements of app
-------------------

1) firebase push:

```bash
add FIREBASE_SERVER_KEY to the settings
add field firebase_token to user model:
firebase_token = models.CharField(_('Firebase token'), default='', blank=True, max_length=500)


```

2) email address to be used as sender email in emails

```bash
add ADMIN_EMAIL to the settings
```

3) kavenegar

```bash
add KAVENEGAR_API to the settings
```

4) pushe push

```bash
add PUSHE_TOKEN to the settings
add ANDROID_PACKAGE_NAME to the settings

add field firebase_token to user model:
pushe_id = models.CharField(_('Pushe id'), default='', blank=True, max_length=500)


```

App abilities
-------------
1) save or update user's firebase token
2) send push with firebase api, save firebase' successful push history
3) send sms with kavenegar api


Usage in code
-------------

send notif after a certain action (action key: my_custom_key)
first declare the key in admin /admin/notifs/notifconfig/
then use this code:

```python
send_notif(key='', recipient_type='', recipient=user, extra_data={}, body_values=[])

```

for single notif:

```python
send_notif(key='my_custom_key', recipient_type='single', recipient=user_obj, extra_data={}, body_values=['my variant data'])
```


for multiple notif:

```python
send_notif(key='my_custom_key2', recipient_type='multiple', recipient=users_qs, extra_data={}, body_values=['my variant data'])
```

more customization:

```python
obj = NotifConfig(key='admin_custom',
                  custom_title='my title',
                  custom_body='my body,
                  pushe=True,
                  is_enable=True)


sent, message = send_notif(key='admin_custom', recipient_type='single', recipient=user,
                           extra_data={}, body_values=[], notif_config=obj)

```